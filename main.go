package main

import (
	"encoding/csv"
	"flag"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	list1Arg := flag.String("list1", "magenta,yellow,peach,orange,green,blue", "List 1 of items as a string without spaces, separated by a comma.")
	list2Arg := flag.String("list2", "rose,daisy,lily,orchid", "List 2 of items as a string without spaces, separated by a comma.")

	flag.Parse()

	list1 := strings.Split(*list1Arg, ",")
	list2 := strings.Split(*list2Arg, ",")

	mergedList := mergeTwoLists(list1, list2)
	if len(mergedList) != 0 {
		fmt.Println(mergedList)
	}

	editTSV()
}

func mergeTwoLists(list1, list2 []string) []string {
	//  Given two lists, write a function that merges corresponding elements from each list into a
	//  new list, with elements from the first list preceding elements from the second list. For
	//  lists of different lengths, list the remaining elements continuously.

	//  Put the lengths of each list into a variable
	list1Length := len(list1)
	list2Length := len(list2)

	// Slice is of indeterminate length- if the two slices are of varying length, then tack on the
	// extras at the end, regardless of which list the elements come from
	mergedList := []string{}

	// check for empty list 1
	emptyList1 := len(list1[0]) == 0

	// Iterate over the first list if there are any elements in it,
	// adding the second list's elements after the first's elements
	// where safe
	if !emptyList1 {
		for index, element := range list1 {
			var mergedString string

			// If index is safe for the list2 param, add the element from list2 at that index
			if index < list2Length {
				mergedString = element + " " + list2[index]
			} else {
				mergedString = element
			}

			//  append the merged string to the end of the list
			mergedList = append(mergedList, mergedString)
		}
	}

	// Handle the cases where there are more elements in list2
	if list2Length > list1Length {
		//  start from zero if the list is empty, if the list is not empty, start from the end of
		//  the first list
		var list2Start int
		if !emptyList1 {
			list2Start = list1Length
		}

		// find the remaining elements and pull them into a variable, then add them to the end of the
		// merged array
		remainingIndices := list2[list2Start:list2Length]
		mergedList = append(mergedList, remainingIndices...)
	}

	// Make the output pretty and readable
	for index := range mergedList {
		//  Find the last index to check for adding commas to the end of merged list1 and list2 elements
		lastIndex := len(mergedList) - 1
		//  add the comma to show the separate array entries when it prints in the console
		if index != lastIndex {
			//  edit the slice using pointer semantics so the changes
			//  are reflected in the returned slice
			mergedList[index] += ","
		}
	}

	return mergedList
}

func editTSV() {
	//  Given the input table (tab-delimited), for each key in colA, sum all the associated values
	//  in colB. Report keys with sums. Keys should be reported in the reverse order of initial
	//  appearance in the input. Do not hard code the lists in your code but read and write
	//  from/to tab-delimited files

	//  Make a map to track the sums
	sumMap := make(map[string]int)
	//  Make a slice to track the order of keys
	keyOrder := []string{}

	//  Assume the input file is in the same directory as the code
	tsvFile, err := os.Open("./code_test_input.tsv")
	if err != nil {
		fmt.Println(err)
	}

	// Read the input, change the delimiter to tabs (default is comma)
	reader := csv.NewReader(tsvFile)
	reader.Comma = '\t'

	// Get the data out of the tsv file into a slice of slices
	tsvData, err := reader.ReadAll()
	if err != nil {
		fmt.Println(err)
	}

	//  Headers don't change so read the data from the 2nd element on
	for _, rows := range tsvData[1:] {
		//  get the key from the first column
		key := rows[0]

		//  parse the int value from the 2nd column so we can do the sums,
		//  ignore the error, we have faith in the input data ;)
		value, _ := strconv.Atoi(rows[1])

		//  check for the value of a key in the map, if it doesn't exist,
		//  add it to the key order array
		if _, ok := sumMap[key]; !ok {
			keyOrder = append(keyOrder, key)
		}

		//  add the value to the sum
		sumMap[key] += value
	}

	//  open file with create and write permissions
	file, err := os.OpenFile("output.tsv", os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		fmt.Println("failed creating file: %s", err)
	}
	//  make a new csv writer and give it the empty file
	tsvWriter := csv.NewWriter(file)
	//  change the delimiter to the tab escape sequence
	tsvWriter.Comma = '\t'

	//  Write the column headers to start
	tsvWriter.Write(tsvData[0])

	//  Do a simple reverse for loop to write the lines
	//  in the reverse of the order they're in
	for i := len(keyOrder) - 1; i >= 0; i-- {
		//  get the key from the index lookup of the keyOrder slice
		key := keyOrder[i]
		//  prepare the row to write to tsv, add the key to it
		rowToWrite := []string{key}
		//  parse the string from the int
		stringValue := strconv.Itoa(sumMap[key])
		//  append the string representation of the sum to the slice to write
		rowToWrite = append(rowToWrite, stringValue)

		//  write the row
		tsvWriter.Write(rowToWrite)
	}
	//  flush and close the writer and file respectively
	tsvWriter.Flush()
	tsvFile.Close()
}
